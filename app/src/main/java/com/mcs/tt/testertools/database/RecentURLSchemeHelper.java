package com.mcs.tt.testertools.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mcs.tt.testertools.model.RecentURLScheme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Maxim on 8/1/2015.
 */
public class RecentURLSchemeHelper extends SQLiteOpenHelper {
    static final String DATABASE_NAME = "url_schemes.db";
    static int SCHEMA_VERSION = 2;

    public final String FIELD_DATE = "date";
    public final String FIELD_URL = "url";
    public final String FIELD_COUNT = "count";

    Context context;
    static RecentURLSchemeHelper instance = null;

    synchronized public static RecentURLSchemeHelper getInstance(Context context)
    {
        if (instance == null)
            instance = new RecentURLSchemeHelper(context.getApplicationContext());

        return instance;
    }

    RecentURLSchemeHelper(Context context)
    {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.beginTransaction();
            db.execSQL("CREATE TABLE url_schemes " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + FIELD_URL + " TEXT, "
                    + FIELD_COUNT + " INTEGER, "
                    + FIELD_DATE + " LONG"
                    + ")");
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.beginTransaction();
            if (oldVersion == 1 && newVersion == 2)
            {
                db.execSQL("ALTER TABLE url_schemes add column " + FIELD_COUNT + " INTEGER");
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<RecentURLScheme> getURLSchemes()
    {
        ArrayList<RecentURLScheme> result = new ArrayList<>();
        Cursor cur = null;
        try {
            cur = getReadableDatabase().rawQuery(
                    "select id, "
                            + FIELD_URL + ", "
                            + FIELD_COUNT + ", "
                            + FIELD_DATE + " from url_schemes",
                    null);

            while (cur.moveToNext()) {
                int dateIndex = cur.getColumnIndex(FIELD_DATE);
                long dateValue = cur.getLong(dateIndex);
                RecentURLScheme recentURLScheme = new RecentURLScheme(
                        cur.getString(cur.getColumnIndex(FIELD_URL)),
                        cur.getInt(cur.getColumnIndex(FIELD_COUNT)),
                        new Date(dateValue)
                );
                result.add(recentURLScheme);
            }
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        } finally {
            if (cur != null)
                cur.close();
        }

        return result;
    }

    public void setURLSchemes(ArrayList<RecentURLScheme> uRLSchemes)
    {
        getWritableDatabase().beginTransaction();
        getWritableDatabase().execSQL("delete from url_schemes");

        for (RecentURLScheme recentURLScheme: uRLSchemes) {
            Object[] args = {
                    recentURLScheme.getScheme(),
                    recentURLScheme.getUsingCount(),
                    recentURLScheme.getUsingDate().getTime()
            };
            getWritableDatabase().execSQL(
                    "INSERT INTO url_schemes ("
                            + FIELD_URL + ", "
                            + FIELD_COUNT + ", "
                            + FIELD_DATE + ") values (?, ?, ?)",
                    args
            );
        }

        getWritableDatabase().setTransactionSuccessful();
        getWritableDatabase().endTransaction();
    }
}
