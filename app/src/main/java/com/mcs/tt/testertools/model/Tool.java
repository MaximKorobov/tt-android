package com.mcs.tt.testertools.model;

/**
 * Created by Home on 22.07.2015.
 */
public interface Tool {
    String getName();
    String getDescription();

    Class<?> getActivityClass();
}
