package com.mcs.tt.testertools.model;

/**
 * Created by Maxim on 7/25/2015.
 */
public class PopularURLScheme {
    private String scheme;
    private String application;
    private String applicationRu;

    public PopularURLScheme(String scheme, String application, String applicationRu) {
        this.scheme = scheme;
        this.application = application;
        this.applicationRu = applicationRu;
    }

    public PopularURLScheme(String[] data) {
        this(data[0], data[1], data[2]);
    }

    public String getScheme() {
        return scheme;
    }

    public String getApplication() {
        return application;
    }

    public String getApplicationRu() {
        return applicationRu;
    }
}
