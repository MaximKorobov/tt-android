package com.mcs.tt.testertools.tasks;

/**
 * Created by Maxim on 8/1/2015.
 */
public class CreateContactsParams {
    public int count;
    public boolean withPhones;
    public boolean withPhotos;
}
