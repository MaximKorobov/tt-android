package com.mcs.tt.testertools.ui.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mcs.tt.testertools.model.Tool;

import java.util.List;

/**
 * Created by Home on 22.07.2015.
 */
public class ToolsAdapter extends ArrayAdapter<Tool> {
    private final List<Tool> list;
    private final Activity context;

    public ToolsAdapter(Activity context, List<Tool> list) {
        super(context, android.R.layout.simple_list_item_2);

        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected TextView name;
        protected TextView description;
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(android.R.layout.simple_list_item_2, null);

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(android.R.id.text1);
            viewHolder.description = (TextView) view.findViewById(android.R.id.text2);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(list.get(position).getName());
        holder.description.setText(list.get(position).getDescription());
        return view;
    }

    public void showActivity(int index)
    {
        Class<?> activityClass = list.get(index).getActivityClass();

        Intent intent = new Intent(context, activityClass);
        context.startActivity(intent);
    }
}
