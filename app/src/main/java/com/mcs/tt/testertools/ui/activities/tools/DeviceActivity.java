package com.mcs.tt.testertools.ui.activities.tools;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.tools.Clipboard;
import com.mcs.tt.testertools.ui.adapters.DeviceInfoAdapter;

/**
 * Created by Maxim on 8/23/2015.
 */
public class DeviceActivity extends AppCompatActivity {

    ListView lvDevice;

    DeviceInfoAdapter deviceInfoAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_device);

        Button btnCopyToClipboard = (Button)findViewById(R.id.btnCopyToClipboard);
        btnCopyToClipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = deviceInfoAdapter.getDataToCopy();
                CopyText(text);
            }
        });

        deviceInfoAdapter = new DeviceInfoAdapter(this);

        lvDevice = (ListView)findViewById(R.id.lvDevice);
        lvDevice.setAdapter(deviceInfoAdapter);

        lvDevice.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String text = deviceInfoAdapter.getDataToCopy(i);
                CopyText(text);
            }
        });
    }

    private void CopyText(String text)
    {
        Clipboard clipboard = new Clipboard(DeviceActivity.this);
        clipboard.copyToClipboard(text, getString(R.string.device_copy_title));

        text = getString(R.string.text_copied)
                + getString(R.string.common_new_line)
                + text;
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
}
