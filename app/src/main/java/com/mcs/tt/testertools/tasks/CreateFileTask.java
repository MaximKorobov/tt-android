package com.mcs.tt.testertools.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.tools.Formatter;
import com.mcs.tt.testertools.tools.RawData;
import com.mcs.tt.testertools.ui.activities.tools.StorageActivity;
import com.sromku.simple.storage.Storage;

import java.io.ByteArrayOutputStream;

/**
 * Created by Maxim on 8/1/2015.
 */
public class CreateFileTask extends AsyncTask<Void, Long, Boolean>
{
    Storage storage;
    String folder;
    String fileName;
    long totalSize;

    StorageActivity activity;
    ProgressDialog progressDialog;

    public CreateFileTask(StorageActivity activity, Storage storage, String folder,
                          String fileName, long totalSize)
    {
        this.activity = activity;

        this.storage = storage;
        this.folder = folder;
        this.fileName = fileName;
        this.totalSize = totalSize;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            ByteArrayOutputStream block = RawData.getStream(activity, R.raw.lorem_ipsum);

            long blockSize = block.size();
            long writeSize = 0;

            if (!storage.isDirectoryExists(folder))
                storage.createDirectory(folder);

            while (writeSize < totalSize) {
                if (storage.isFileExist(folder, fileName))
                    storage.appendFile(folder, fileName, block.toByteArray());
                else
                    storage.createFile(folder, fileName, block.toByteArray());

                writeSize = writeSize + blockSize;

                publishProgress(writeSize);
            }
            publishProgress(totalSize);

            return true;
        } catch (Exception e){
            return false;
        }
    }

    protected void onProgressUpdate(Long... progress) {
        String progressText = activity.getResources().getString(R.string.storage_create_progress);

        progressDialog.setMessage(String.format(
                progressText,
                Formatter.bytesToMegabytes(progress[0]),
                Formatter.bytesToMegabytes(totalSize)));

        progressDialog.setProgress((int)(progress[0] / totalSize));
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        progressDialog.dismiss();
        activity.UpdateAsync();
    }
}