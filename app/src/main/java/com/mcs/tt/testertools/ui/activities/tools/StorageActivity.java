package com.mcs.tt.testertools.ui.activities.tools;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.tasks.CreateFileTask;
import com.mcs.tt.testertools.tools.Formatter;
import com.sromku.simple.storage.SimpleStorage;
import com.sromku.simple.storage.Storage;
import com.sromku.simple.storage.helpers.SizeUnit;

import java.io.File;

public class StorageActivity extends AppCompatActivity {

    final String defaultFolder = "TesterTools";
    final String defaultFileName = "stub.txt";

    RadioButton rbExternalStorage;
    RadioButton rbInternalStorage;

    TextView tvTotalSize;
    TextView tvAvailableSize;
    TextView tvMaxValue;
    SeekBar sbSelector;
    EditText etValue;

    TextView tvFileExists;
    ImageButton ibFilePath;

    Button btnCreateFile;
    Button btnDeleteFile;

    Storage internalStorage;
    Storage externalStorage;

    int fileSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);

        rbExternalStorage = (RadioButton)findViewById(R.id.rbExternalStorage);
        rbExternalStorage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    updateStorage(externalStorage);
            }
        });
        rbInternalStorage = (RadioButton)findViewById(R.id.rbInternalStorage);
        rbInternalStorage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    updateStorage(internalStorage);
            }
        });
        tvTotalSize = (TextView)findViewById(R.id.tvTotalSize);
        tvAvailableSize = (TextView)findViewById(R.id.tvAvailableSize);

        btnCreateFile = (Button)findViewById(R.id.btnCreateFile);
        btnCreateFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createFile(getStorage(), fileSize);
            }
        });
        btnDeleteFile = (Button)findViewById(R.id.btnDeleteFile);
        btnDeleteFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteFile(getStorage());
            }
        });

        TextView tvValue = (TextView)findViewById(R.id.tvValue);
        tvValue.setText(R.string.storage_file_size);
        etValue = (EditText)findViewById(R.id.etValue);
        etValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    fileSize = Integer.parseInt(charSequence.toString());
                } catch (NumberFormatException e) {
                    fileSize = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        sbSelector = (SeekBar)findViewById(R.id.sbSelector);
        sbSelector.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                etValue.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        tvMaxValue = (TextView)findViewById(R.id.tvMaxValue);
        tvMaxValue.setText(R.string.storage_available_size);

        tvFileExists = (TextView)findViewById(R.id.tvFileExists);

        ibFilePath = (ImageButton)findViewById(R.id.ibFilePath);
        ibFilePath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filePath = getFilePath(getStorage());
                String text = String.format(getString(R.string.storage_file_path), filePath);
                AlertDialog builder = new AlertDialog.Builder(StorageActivity.this)
                        .setMessage(text)
                        .setTitle(R.string.title_activity_storage)
                        .setPositiveButton(R.string.common_ok, null)
                        .create();
                builder.show();
            }
        });

        internalStorage = SimpleStorage.getInternalStorage(this);
        externalStorage = SimpleStorage.getExternalStorage();
        initStorage();
    }

    void initStorage()
    {
        if (SimpleStorage.isExternalStorageWritable())
            rbExternalStorage.setChecked(true);
        else {
            rbInternalStorage.setChecked(true);
            rbExternalStorage.setEnabled(false);
        }

        updateStorage(getStorage());
    }

    void updateStorage(Storage storage)
    {
        long freeSpace = storage.getFreeSpace(SizeUnit.MB);
        long usedSpace = storage.getUsedSpace(SizeUnit.MB);
        long totalSpace = freeSpace + usedSpace;

        tvTotalSize.setText(Formatter.thousandSeparator(totalSpace) + " MB");
        tvAvailableSize.setText(Formatter.thousandSeparator(freeSpace) + " MB");

        tvMaxValue.setText(tvAvailableSize.getText());

        sbSelector.setMax((int) freeSpace);
        sbSelector.setProgress((int) (freeSpace / 10));
        sbSelector.postInvalidate();

        double fileSize = fileSize(storage);

        if (fileSize != 0) {
            tvFileExists.setText(String.format(getString(R.string.storage_file_exists), fileSize));
            ibFilePath.setVisibility(View.VISIBLE);
        } else {
            tvFileExists.setText(R.string.storage_file_not_exists);
            ibFilePath.setVisibility(View.INVISIBLE);
        }
    }

    Storage getStorage()
    {
        Storage result;

        if (rbExternalStorage.isChecked())
            result = externalStorage;
        else
            result = internalStorage;

        return result;
    }

    String getFolder()
    {
        return defaultFolder;
    }

    String getFileName()
    {
        return defaultFileName;
    }

    File getFile(Storage storage)
    {
        File file = storage.getFile(getFolder(), getFileName());
        return file;
    }

    String getFilePath(Storage storage)
    {
        File file = getFile(storage);
        return file.getAbsolutePath();
    }

    double fileSize(Storage storage)
    {
        File file = getFile(storage);
        return storage.getSize(file, SizeUnit.MB);
    }

    private void createFile(Storage storage, int megabytes) {
        String folder = getFolder();
        String fileName = getFileName();

        long size = (long)megabytes * 1024 * 1024;

        new CreateFileTask(StorageActivity.this, storage, folder, fileName, size).execute();
    }

    public void UpdateAsync()
    {
        updateStorage(getStorage());
    }

    private void deleteFile(Storage storage)
    {
        storage.deleteFile(getFolder(), getFileName());
        updateStorage(storage);
    }
}
