package com.mcs.tt.testertools.ui.activities.tools;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TabHost;
import android.widget.TextView;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.tasks.AsyncTaskListener;
import com.mcs.tt.testertools.tasks.CreateContactsParams;
import com.mcs.tt.testertools.tasks.CreateContactsTask;
import com.mcs.tt.testertools.tasks.DeleteContactsTask;
import com.mcs.tt.testertools.tools.Contacts;

/**
 * Created by Maxim on 7/26/2015.
 */
public class ContactsActivity extends AppCompatActivity implements AsyncTaskListener {

    public static final String TAG_CREATE = "tagCreate";
    public static final String TAG_DELETE = "tagDelete";
    final int defaultCreateCount = 100;
    final int maxCreateCount = 1000;

    Contacts contacts;

    TextView tvContactsCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        contacts = new Contacts(ContactsActivity.this);

        // Create contacts tab
        TextView tvValue = (TextView)findViewById(R.id.tvValue);
        tvValue.setText(R.string.contacts_selector_value);

        TextView textMax = (TextView)findViewById(R.id.tvMaxValue);
        textMax.setText(String.valueOf(maxCreateCount));
        textMax.postInvalidate();

        final EditText etValue = (EditText)findViewById(R.id.etValue);

        SeekBar sbValue = (SeekBar)findViewById(R.id.sbSelector);
        sbValue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                etValue.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        sbValue.setMax(maxCreateCount);
        sbValue.setProgress(defaultCreateCount);
        sbValue.postInvalidate();

        final CheckBox cbCreateOptionPhones = (CheckBox)findViewById(R.id.cbCreateOptionPhones);
        final CheckBox cbCreateOptionPhoto = (CheckBox)findViewById(R.id.cbCreateOptionPhoto);

        Button btnCreateContacts = (Button)findViewById(R.id.btnCreateContacts);
        btnCreateContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateContactsParams createContactsParams = new CreateContactsParams();
                try {
                    createContactsParams.count = Integer.parseInt(etValue.getText().toString());
                } catch (NumberFormatException e) {
                    createContactsParams.count = 0;
                }
                createContactsParams.withPhones = cbCreateOptionPhones.isChecked();
                createContactsParams.withPhotos = cbCreateOptionPhoto.isChecked();

                new CreateContactsTask(ContactsActivity.this, ContactsActivity.this, contacts)
                        .execute(createContactsParams);
            }
        });

        // Delete contacts tab
        tvContactsCount = (TextView)findViewById(R.id.tvContactsCount);
        Button btnDeleteAllContacts = (Button)findViewById(R.id.btnDeleteAllContacts);
        btnDeleteAllContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteContactsTask(ContactsActivity.this, ContactsActivity.this)
                        .execute();
            }
        });

        // Tabs
        TabHost thContacts = (TabHost)findViewById(R.id.thContacts);
        thContacts.setup();

        TabHost.TabSpec spec = thContacts.newTabSpec(TAG_CREATE);
        spec.setContent(R.id.tab1);
        spec.setIndicator(getString(R.string.contacts_tab_create));
        thContacts.addTab(spec);

        spec = thContacts.newTabSpec(TAG_DELETE);
        spec.setContent(R.id.tab2);
        spec.setIndicator(getString(R.string.contacts_tab_delete));
        thContacts.addTab(spec);
        thContacts.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (tabId.equals(TAG_DELETE))
                    updateUI();
            }
        });
    }

    @Override
    public void updateUI()
    {
        int contactsCount = contacts.getContactsCount();
        tvContactsCount.setText(
                String.format(getString(R.string.contacts_delete_count), contactsCount)
        );
    }
}
