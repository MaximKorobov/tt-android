package com.mcs.tt.testertools.model;

import java.util.Date;

/**
 * Created by Maxim on 8/1/2015.
 */
public class RecentURLScheme {
    private String scheme;
    private Date usingDate;
    private Integer usingCount;

    public RecentURLScheme(String scheme, Integer usingCount, Date usingDate) {
        this.scheme = scheme;
        this.usingCount = usingCount;
        this.usingDate = usingDate;
    }

    public String getScheme() {
        return scheme;
    }

    public Integer getUsingCount() {
        return usingCount;
    }

    public Date getUsingDate() {
        return usingDate;
    }
}
