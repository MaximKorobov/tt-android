package com.mcs.tt.testertools.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.mcs.tt.testertools.R;

/**
 * Created by Maxim on 8/1/2015.
 */
public class DeleteContactsTask extends AsyncTask<Void, Integer, Boolean>
{
    private final AsyncTaskListener listener;
    Activity activity;

    ProgressDialog progressDialog;

    public DeleteContactsTask(Activity activity, AsyncTaskListener listener)
    {
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        ContentResolver cr = activity.getContentResolver();
        Cursor cur = null;
        try {
            cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            int count = cur.getCount();
            int deleted = 0;

            while (cur.moveToNext()) {
                String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
                cr.delete(uri, null, null);
                publishProgress(deleted++, count);
            }

            return true;
        } catch (Exception e){
            return false;
        } finally {
            if (cur != null)
                cur.close();
        }
    }

    protected void onProgressUpdate(Integer... progress) {
        String progressText = activity.getResources().getString(R.string.contacts_delete_progress);
        progressDialog.setMessage(String.format(progressText, progress[0], progress[1]));
        progressDialog.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        progressDialog.dismiss();
        listener.updateUI();
    }
}