package com.mcs.tt.testertools.tools;

/**
 * Created by Home on 29.07.2015.
 */
public class Formatter {

    public static String thousandSeparator(long number)
    {
        return String.format("%,d", number).replace(",", " ");
    }

    public static long bytesToMegabytes(long bytes)
    {
        long sizeInMb = bytes / (1024 * 1024);
        return sizeInMb;
    }
}
