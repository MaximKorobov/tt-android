package com.mcs.tt.testertools.ui.activities.tools;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.database.RecentURLSchemeHelper;
import com.mcs.tt.testertools.model.PopularURLScheme;
import com.mcs.tt.testertools.model.RecentURLScheme;
import com.mcs.tt.testertools.tools.Locales;
import com.mcs.tt.testertools.tools.RawData;
import com.mcs.tt.testertools.ui.adapters.PopularURLSchemesAdapter;
import com.mcs.tt.testertools.ui.adapters.RecentURLSchemesAdapter;

import java.util.ArrayList;

public class URLSchemeActivity extends AppCompatActivity {

    final char URLSchemeSuffix = ':';
    ArrayList<RecentURLScheme> recentURLSchemes;
    ArrayList<PopularURLScheme> popularPopularURLSchemes;

    SharedPreferences preferences;
    final String PREFERENCE_RECENT_URL = "PREFERENCE_RECENT_URL";

    RecentURLSchemeHelper recentURLSchemeHelper;

    RecentURLSchemesAdapter recentUrlSchemesAdapter;
    PopularURLSchemesAdapter popularUrlSchemesAdapter;

    EditText etURLScheme;
    ListView listViewURLSchemes;
    RadioButton rbRecentItems;
    RadioButton rbPopularItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url_scheme);

        preferences = getPreferences(MODE_PRIVATE);

        recentURLSchemeHelper = RecentURLSchemeHelper.getInstance(this.getApplicationContext());

        etURLScheme = (EditText)findViewById(R.id.etURLScheme);

        Button btnGo = (Button)findViewById(R.id.btnGo);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String baseText = etURLScheme.getText().toString();

                String text = baseText;
                if (text.isEmpty()) {
                    Toast.makeText(
                            getApplicationContext(),
                            String.format(getString(R.string.url_scheme_error_empty), text),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if (text.charAt(text.length() - 1) != URLSchemeSuffix)
                    text += URLSchemeSuffix;

                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = Uri.parse(text);
                intent.setData(uri);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(
                            getApplicationContext(),
                            String.format(getString(R.string.url_scheme_error), text),
                            Toast.LENGTH_SHORT)
                            .show();
                }

                addRecentItem(baseText);
            }
        });

        rbRecentItems = (RadioButton)findViewById(R.id.rbRecentItems);
        rbPopularItems = (RadioButton)findViewById(R.id.rbPopularItems);

        recentURLSchemes = getRecentURLSchemes();
        popularPopularURLSchemes = getPopularPopularURLSchemes();

        listViewURLSchemes = (ListView)findViewById(R.id.lvURLSchemes);
        listViewURLSchemes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (rbRecentItems.isChecked())
                    etURLScheme.setText(recentUrlSchemesAdapter.getURLScheme(i).getScheme());
                else
                    etURLScheme.setText(popularUrlSchemesAdapter.getURLScheme(i).getScheme());
                etURLScheme.postInvalidate();
            }
        });
        listViewURLSchemes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (rbRecentItems.isChecked()) {
                    final int pos = position;
                    AlertDialog builder = new AlertDialog.Builder(URLSchemeActivity.this)
                            .setTitle(R.string.common_confirm)
                            .setMessage(R.string.url_scheme_delete_confirm)
                            .setPositiveButton(R.string.common_yes,
                                    new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    recentUrlSchemesAdapter.delete(pos);
                                }
                            })
                            .setNegativeButton(R.string.common_no, null)
                            .create();
                    builder.show();
                    return true;
                }
                return false;
            }
        });

        initURLSchemesList();

        rbRecentItems.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    listViewURLSchemes.setAdapter(recentUrlSchemesAdapter);
            }
        });

        rbPopularItems.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    listViewURLSchemes.setAdapter(popularUrlSchemesAdapter);
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void addRecentItem(String baseText) {
        recentUrlSchemesAdapter.insertOrUpdate(baseText);
    }

    private void initURLSchemesList() {
        recentURLSchemes = recentURLSchemeHelper.getURLSchemes();
        recentUrlSchemesAdapter = new RecentURLSchemesAdapter(this);
        recentUrlSchemesAdapter.setData(recentURLSchemes);

        popularUrlSchemesAdapter = new PopularURLSchemesAdapter(this, Locales.isRusLocale(this));
        popularUrlSchemesAdapter.setData(popularPopularURLSchemes);

        if (!recentURLSchemes.isEmpty())
        {
            rbRecentItems.setChecked(true);
            listViewURLSchemes.setAdapter(recentUrlSchemesAdapter);
        }
        else
        {
            rbPopularItems.setChecked(true);
            listViewURLSchemes.setAdapter(popularUrlSchemesAdapter);
        }
    }

    ArrayList<PopularURLScheme> getPopularPopularURLSchemes()
    {
        ArrayList<PopularURLScheme> result = new ArrayList<>();
        String data = RawData.getText(this, R.raw.url_schemes);
        String[] uRLSchemesTable = data.split("\n");

        for (String anURLSchemeRow : uRLSchemesTable) {
            String[] uRLSchemeData = anURLSchemeRow.split(",");
            PopularURLScheme popularUrlScheme = new PopularURLScheme(uRLSchemeData);
            result.add(popularUrlScheme);
        }

        return result;
    }

    ArrayList<RecentURLScheme> getRecentURLSchemes()
    {
        return new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String recentURL = preferences.getString(PREFERENCE_RECENT_URL, null);
        etURLScheme.setText(recentURL);
    }

    @Override
    protected void onStop() {
        super.onStop();

        String recentURL = etURLScheme.getText().toString();
        preferences.edit().putString(PREFERENCE_RECENT_URL, recentURL).apply();

        recentURLSchemeHelper.setURLSchemes(recentURLSchemes);
    }
}
