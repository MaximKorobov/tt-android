package com.mcs.tt.testertools.model;

/**
 * Created by Maxim on 8/23/2015.
 */
public class DeviceInfo {
    public String name;
    public String value;

    public  DeviceInfo(String name, String value)
    {
        this.name = name;
        this.value = value;
    }
}
