package com.mcs.tt.testertools.tools;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.model.DeviceInfo;

/**
 * Created by Home on 04.10.2015.
 */
public class DeviceInformation {

    private final Activity activity;

    public DeviceInformation(Activity activity)
    {
        this.activity = activity;
    }

    public DeviceInfo getAPILevel()
    {
        DeviceInfo APILevel = new DeviceInfo(
                activity.getString(R.string.device_android_version),
                String.format(activity.getString(R.string.device_android_version_value),
                        Build.VERSION.RELEASE,
                        Build.VERSION.CODENAME,
                        Build.VERSION.SDK_INT)
        );

        return APILevel;
    }

    public DeviceInfo getAndroidId() {
        DeviceInfo androidId = new DeviceInfo(
                activity.getString(R.string.device_android_id),
                Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID)
        );

        return androidId;
    }

    public DeviceInfo getIMEI() {
        TelephonyManager telephonyManager =
                (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        DeviceInfo imei = new DeviceInfo(
                activity.getString(R.string.device_imei),
                telephonyManager.getDeviceId()
        );

        return imei;
    }

    public DeviceInfo getMobilePhone() {
        TelephonyManager telephonyManager =
                (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        String phone = telephonyManager.getLine1Number();
        DeviceInfo mobilePhone = new DeviceInfo(
                activity.getString(R.string.device_phone_number),
                (phone == null || phone.isEmpty()) ? phone : activity.getString(R.string.device_phone_number_unknown)
        );

        return mobilePhone;
    }

    public DeviceInfo getIsRooted() {
        Boolean isRooted = Root.isDeviceRooted();
        DeviceInfo isRootedDevice = new DeviceInfo(
                activity.getString(R.string.device_is_rooted),
                isRooted ? activity.getString(R.string.device_is_rooted_value_yes)
                        : activity.getString(R.string.device_is_rooted_value_no)
        );

        return isRootedDevice;
    }
}
