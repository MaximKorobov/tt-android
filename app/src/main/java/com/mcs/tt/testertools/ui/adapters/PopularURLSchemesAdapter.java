package com.mcs.tt.testertools.ui.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mcs.tt.testertools.model.PopularURLScheme;

import java.util.List;

/**
 * Created by Maxim on 7/25/2015.
 */
public class PopularURLSchemesAdapter extends ArrayAdapter<PopularURLScheme> {
    List<PopularURLScheme> list;
    Activity activity;
    boolean isRusLocale;

    public PopularURLSchemesAdapter(Activity context, boolean isRusLocale) {
        super(context, android.R.layout.simple_list_item_2);

        this.activity = context;
        this.isRusLocale = isRusLocale;
    }

    public void setData(List<PopularURLScheme> list)
    {
        this.list = list;
    }

    static class ViewHolder {
        protected TextView name;
        protected TextView application;
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(android.R.layout.simple_list_item_2, null);

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(android.R.id.text1);
            viewHolder.application = (TextView) view.findViewById(android.R.id.text2);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(list.get(position).getScheme());
        String application = isRusLocale ?
                list.get(position).getApplicationRu() :
                list.get(position).getApplication();
        holder.application.setText(application);
        return view;
    }

    public PopularURLScheme getURLScheme(int index)
    {
        return list.get(index);
    }
}
