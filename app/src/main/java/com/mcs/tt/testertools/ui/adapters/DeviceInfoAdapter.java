package com.mcs.tt.testertools.ui.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mcs.tt.testertools.model.DeviceInfo;
import com.mcs.tt.testertools.tools.DeviceInformation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maxim on 8/23/2015.
 */
public class DeviceInfoAdapter extends ArrayAdapter<DeviceInfo> {

    Activity activity;
    List<DeviceInfo> deviceInfoList;

    public DeviceInfoAdapter(Activity activity) {
        super(activity, android.R.layout.simple_list_item_2);

        this.activity = activity;

        updateData();
    }

    private void updateData() {
        DeviceInformation deviceInformation = new DeviceInformation(activity);

        deviceInfoList = new ArrayList<>();
        deviceInfoList.add(deviceInformation.getAPILevel());
        deviceInfoList.add(deviceInformation.getAndroidId());
        deviceInfoList.add(deviceInformation.getIMEI());
        deviceInfoList.add(deviceInformation.getMobilePhone());
        deviceInfoList.add(deviceInformation.getIsRooted());
    }

    @Override
    public int getCount() {
        return deviceInfoList.size();
    }

    static class ViewHolder {
        protected TextView name;
        protected TextView value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(android.R.layout.simple_list_item_2, null);

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(android.R.id.text1);
            viewHolder.value = (TextView) view.findViewById(android.R.id.text2);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(deviceInfoList.get(position).name);
        holder.value.setText(deviceInfoList.get(position).value);
        return view;

    }

    public String getDataToCopy()
    {
        String result = "";

        for (DeviceInfo deviceInfo: deviceInfoList)
        {
            if (!result.isEmpty())
                result += "\n";
            result += deviceInfo.name + ": " + deviceInfo.value;
        }

        return result;
    }

    public String getDataToCopy(int index)
    {
        String result = "";

        DeviceInfo deviceInfo = deviceInfoList.get(index);
        result += deviceInfo.name + ": " + deviceInfo.value;

        return result;
    }
}
