package com.mcs.tt.testertools.tools;

import android.content.Context;

import java.util.Locale;

/**
 * Created by Home on 30.07.2015.
 */
public class Locales {
    public static boolean isRusLocale(Context context)
    {
        Locale current = context.getResources().getConfiguration().locale;
        String language = current.getLanguage();

        return language.toLowerCase().equals("ru");
    }
}
