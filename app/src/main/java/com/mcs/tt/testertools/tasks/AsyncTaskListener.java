package com.mcs.tt.testertools.tasks;

/**
 * Created by Maxim on 8/18/2015.
 */
public interface AsyncTaskListener {
    void updateUI();
}
