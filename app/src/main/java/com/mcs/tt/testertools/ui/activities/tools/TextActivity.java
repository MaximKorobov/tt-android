package com.mcs.tt.testertools.ui.activities.tools;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.tools.Clipboard;
import com.mcs.tt.testertools.tools.RawData;

/**
 * Created by Home on 22.07.2015.
 */
public class TextActivity extends AppCompatActivity {

    int length = 1;
    final int maxLength = 100000;
    final int defaultLength = 5000;
    final int defaultVisibleLength = 100;

    EditText etValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        Button btnCopyToClipboard = (Button) findViewById(R.id.btnCopyToClipboard);
        btnCopyToClipboard.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String text = RawData.getText(TextActivity.this, R.raw.lorem_ipsum);

                int copyLength = length >= text.length() ? text.length() : length;
                String label = text.substring(0, copyLength) + getString(R.string.common_dots);

                Clipboard clipboard = new Clipboard(TextActivity.this);
                clipboard.copyToClipboard(text, label);

                int visibleLength = defaultVisibleLength >= copyLength ? copyLength
                        : defaultVisibleLength;
                String visibleLabel = getString(R.string.text_copied)
                        + getString(R.string.common_new_line)
                        + text.substring(0, visibleLength)
                        + getString(R.string.common_dots);
                Toast.makeText(getApplicationContext(), visibleLabel, Toast.LENGTH_SHORT).show();
            }

        });

        etValue = (EditText) findViewById(R.id.etValue);
        etValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    length = Integer.parseInt(charSequence.toString());
                } catch (NumberFormatException e) {
                    length = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        final SeekBar sbTextSize = (SeekBar) findViewById(R.id.sbSelector);
        sbTextSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                etValue.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        TextView tvValue = (TextView) findViewById(R.id.tvValue);
        tvValue.setText(R.string.text_length);

        TextView textMax = (TextView) findViewById(R.id.tvMaxValue);
        textMax.setText(String.valueOf(maxLength));
        textMax.postInvalidate();

        sbTextSize.setMax(maxLength);
        sbTextSize.setProgress(defaultLength);
        sbTextSize.postInvalidate();
    }
}