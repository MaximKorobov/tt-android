package com.mcs.tt.testertools.ui.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.model.Tool;
import com.mcs.tt.testertools.ui.activities.tools.ContactsActivity;
import com.mcs.tt.testertools.ui.activities.tools.DeviceActivity;
import com.mcs.tt.testertools.ui.activities.tools.StorageActivity;
import com.mcs.tt.testertools.ui.activities.tools.TextActivity;
import com.mcs.tt.testertools.ui.activities.tools.URLSchemeActivity;
import com.mcs.tt.testertools.ui.adapters.ToolsAdapter;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    private ToolsAdapter toolsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolsAdapter = new ToolsAdapter(this, getTools());
        ListView listViewTools = (ListView)findViewById(R.id.listViewTools);
        listViewTools.setAdapter(toolsAdapter);
        listViewTools.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                toolsAdapter.showActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_about) {
            AlertDialog builder = new AlertDialog.Builder(this)
                    .setMessage(R.string.about_note)
                    .setTitle(R.string.about_title)
                    .setPositiveButton(R.string.common_ok, null)
                    .create();
            builder.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ArrayList<Tool> getTools()
    {
        final Tool textTool = new Tool() {
            @Override
            public String getName() {
                return getString(R.string.title_activity_text);
            }

            @Override
            public String getDescription() {
                return getString(R.string.text_description);
            }

            @Override
            public Class<?> getActivityClass() {
                return TextActivity.class;
            }
        };

        final Tool storageTool = new Tool() {
            @Override
            public String getName() {
                return getString(R.string.title_activity_storage);
            }

            @Override
            public String getDescription() {
                return getString(R.string.storage_description);
            }

            @Override
            public Class<?> getActivityClass() {
                return StorageActivity.class;
            }
        };

        final Tool urlSchemeTool = new Tool() {
            @Override
            public String getName() {
                return getString(R.string.title_activity_url_scheme);
            }

            @Override
            public String getDescription() {
                return getString(R.string.url_scheme_description);
            }

            public Class<?> getActivityClass()
            {
                return URLSchemeActivity.class;
            }
        };

        final Tool contactsTool = new Tool() {
            @Override
            public String getName() {
                return getString(R.string.title_activity_contacts);
            }

            @Override
            public String getDescription() {
                return getString(R.string.contacts_description);
            }

            @Override
            public Class<?> getActivityClass() {
                return ContactsActivity.class;
            }
        };

        final Tool deviceTool = new Tool() {
            @Override
            public String getName() {
                return getString(R.string.title_activity_device);
            }

            @Override
            public String getDescription() {
                return getString(R.string.device_description);
            }

            @Override
            public Class<?> getActivityClass() {
                return DeviceActivity.class;
            }
        };


        ArrayList<Tool> tools = new ArrayList<Tool>() {{
            add(textTool);
            add(storageTool);
            add(urlSchemeTool);
            add(contactsTool);
            add(deviceTool);
        }};
        return tools;
    }
}
