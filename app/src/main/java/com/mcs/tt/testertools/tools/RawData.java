package com.mcs.tt.testertools.tools;

import android.app.Activity;
import android.content.res.Resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Maxim on 7/25/2015.
 */
public class RawData {

    public static String getText(Activity activity, int id)
    {
        String result = getStream(activity, id).toString();

        return result;
    }

    public static ByteArrayOutputStream getStream(Activity activity, int id)
    {
        ByteArrayOutputStream result = null;

        Resources r = activity.getResources();
        InputStream is = r.openRawResource(id);
        try {
            result = getOutputStream(is);
            is.close();
        } catch (IOException ignored) {}

        return result;
    }

    static ByteArrayOutputStream getOutputStream(InputStream is) throws IOException
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int i = is.read();
        while (i != -1) {
            byteArrayOutputStream.write(i);
            i = is.read();
        }

        return byteArrayOutputStream;
    }
}
