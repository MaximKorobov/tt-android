package com.mcs.tt.testertools.tools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;

/**
 * Created by Maxim on 8/22/2015.
 */
public class Clipboard {
    Activity activity;

    public Clipboard(Activity activity)
    {
        this.activity = activity;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void copyToClipboard(String text, String label) {
        int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.HONEYCOMB){
            ClipboardManager clipboard = (ClipboardManager)
                    activity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(label, text);
            clipboard.setPrimaryClip(clip);
        } else{
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager)
                    activity.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(label);
        }
    }

}
