package com.mcs.tt.testertools.ui.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mcs.tt.testertools.model.RecentURLScheme;
import com.mcs.tt.testertools.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Maxim on 8/1/2015.
 */
public class RecentURLSchemesAdapter extends ArrayAdapter<RecentURLScheme> {
    List<RecentURLScheme> list;
    Activity activity;

    String NOTE_TEMPLATE;
    Locale currentLocale;

    public RecentURLSchemesAdapter(Activity context) {
        super(context, android.R.layout.simple_list_item_2);

        this.activity = context;
        NOTE_TEMPLATE = context.getString(R.string.url_scheme_item_note);
        currentLocale = context.getResources().getConfiguration().locale;
    }

    public void setData(List<RecentURLScheme> list)
    {
        this.list = list;
    }

    public void delete(int index) {
        list.remove(index);
        notifyDataSetChanged();
    }

    public void insertOrUpdate(String newURLScheme)
    {
        ArrayList<Integer> itemsToDelete = new ArrayList<>();
        int newCount = 0;

        for(int index = 0; index < list.size(); index++)
        {
            RecentURLScheme item = list.get(index);
            if (item.getScheme().equals(newURLScheme))
            {
                itemsToDelete.add(index);
                newCount += item.getUsingCount();
            }
        }

        Collections.reverse(itemsToDelete);
        for (int index = 0; index < itemsToDelete.size(); index++)
            list.remove(itemsToDelete.get(index).intValue());

        RecentURLScheme newItem = new RecentURLScheme(newURLScheme, newCount + 1, new Date());
        list.add(0, newItem);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        protected TextView name;
        protected TextView useDateAndCount;
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            view = inflater.inflate(android.R.layout.simple_list_item_2, null);

            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.name = (TextView) view.findViewById(android.R.id.text1);
            viewHolder.useDateAndCount = (TextView) view.findViewById(android.R.id.text2);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        RecentURLScheme item = list.get(position);

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.name.setText(item.getScheme());

        Date date = item.getUsingDate();
        DateFormat formatter = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.SHORT,
                currentLocale);
        String useDate = formatter.format(date);
        Integer useCount = item.getUsingCount();
        holder.useDateAndCount.setText(String.format(NOTE_TEMPLATE, useCount, useDate));

        return view;
    }

    public RecentURLScheme getURLScheme(int index)
    {
        return list.get(index);
    }
}