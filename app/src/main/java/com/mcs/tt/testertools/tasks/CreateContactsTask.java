package com.mcs.tt.testertools.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.mcs.tt.testertools.R;
import com.mcs.tt.testertools.tools.Contacts;
import com.mcs.tt.testertools.tools.RawData;

import java.io.ByteArrayOutputStream;

/**
 * Created by Maxim on 8/1/2015.
 */
public class CreateContactsTask extends AsyncTask<CreateContactsParams, Integer, Boolean>
{
    Contacts contacts;

    Activity activity;
    private final AsyncTaskListener listener;

    ProgressDialog progressDialog;

    public CreateContactsTask(Activity activity, AsyncTaskListener listener, Contacts contacts)
    {
        this.activity = activity;
        this.listener = listener;
        this.contacts = contacts;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(CreateContactsParams... params) {
        try {

            int count = params[0].count;
            boolean withPhones = params[0].withPhones;
            boolean withPhotos = params[0].withPhotos;
            ByteArrayOutputStream image = null;
            if (withPhotos)
                image = RawData.getStream(activity, R.mipmap.ic_launcher);

            progressDialog.setMax(count);
            for (int i = 0; i < count; i++) {
                publishProgress(i + 1, count);

                String phone = withPhones ? "+7(916)" + String.format("%07d", i) : "";
                contacts.createContact(
                        "Contact " + String.format("%0" + String.valueOf(count).length() + "d", i),
                        phone, image);
            }

            return true;
        } catch (Exception e){
            return false;
        }
    }

    protected void onProgressUpdate(Integer... progress) {
        String progressText = activity.getResources().getString(R.string.contacts_create_progress);
        progressDialog.setMessage(String.format(progressText, progress[0], progress[1]));
        progressDialog.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        progressDialog.dismiss();
        listener.updateUI();
    }
}